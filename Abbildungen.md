# Exportieren von Auftragungen
Aktuell noch Notizenliste

- Achsen sind beschriftet
- Legende
- Fehlerbalken
- Titel
- Symbole möglichst auch S/W trennbar
    - Unterschiedliche Linienstile bei Geraden/Kurven (- -- -. :)
    - Unterschiedliche Symbole
- Farben sollen erkennbar sein -- aber auch nicht zu grell!
- Schriften gerne etwas größer einstellen -- ausgedruckt muss es auch lesbar sein!
- Möglichst Vektorgrafiken wie PDF, EPS oder für LaTeX PGF nutzen
    - Meist wenig Speicherplatz, aber „unendliche“ Auflösung
    - Alternativ: DPI=600 bei PNG stellt Kompromiss aus Auflösung und Dateigröße dar, DPI=1200 für einen sehr hochauflösenden Druck (welche der Drucker aber auch erstmal entsprechend darstellen muss)
- Bei Subplots <code>tight_layout</code> ntuzen 
## Für Fortgeschrittene
- Verweis auf rc_Params aus matplotlib
- Globale Möglichkeit, Einstellungen von Schriftgrößen, der Schrift, etc. einzustellen
- Bei lokaler Installation auch möglich, per pgf LaTeX-Pakete einzubinden und z.B. siunitx für EInheiten zu nutzen
