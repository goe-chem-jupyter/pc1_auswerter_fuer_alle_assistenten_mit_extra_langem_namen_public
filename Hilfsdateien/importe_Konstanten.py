from scipy import constants
#N_A,R,h und k_B könnten auch per from scipy.constants import … as … importiert werden
N_A = constants.value(u'Avogadro constant')
F   = constants.value(u'Faraday constant')
R   = constants.value(u'molar gas constant')
h   = constants.value(u'Planck constant')
k_B = constants.value(u'Boltzmann constant') 
c_0 = constants.value(u'speed of light in vacuum')
g   = constants.value(u'standard acceleration of gravity')
from scipy.constants import torr as torr_Pa

#Studentscher t-Faktor für [N-1] Freiheitsgrade
# 3 Freiheitsgrade --> student_faktor[2]
from numpy import array
student_faktor = array([12.706,4.303,3.182,2.776,2.571,2.447,2.365,2.306,2.262,2.228]) #p=0.95, zweiseitiger Bereich