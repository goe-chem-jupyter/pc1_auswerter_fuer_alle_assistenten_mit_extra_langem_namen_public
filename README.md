In diesem Repository sind Jupyter-Notebooks und Datensätze zur Auswertung der Versuche im Thermopraktikum im B.Sc. Chemie.
Aktuell werten alle _fertigen_ Jupyter-Notebooks einen Datensatz vollständig aus. Die Studierenden werden später eine andere Version erhalten, die von ihnen auszufüllen ist.
